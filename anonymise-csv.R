# Since this anonymises the original data, and only the anonymised data is
# published, the reader won't have access to the original data. It's loaded
# from the local directory

library(dplyr)

anonymise <- function(year) {
  filename <- paste(sep = '', '~/Nextcloud/Work/Data/OriginalCSVFiles/',year,'_survey_data.csv')
  drop <- c('Timestamp',
            'Contact.address..optional.',
            'Contact.email.address..optional..')
  raw_csv <- read.csv(filename)
  raw_csv %>%
    select(-one_of(drop)) %>%
    write.csv(file = paste(sep = '', './',year,'_survey_data.anonymised.csv'),row.names = F)
}

for (year in c('2016','2017','2018')) {
  anonymise(year)
}