library('dplyr')

dir.create('/tmp/csv')

if (!file.exists('./2018-data.csv')) {
  download.file('https://downloads.theforeman.org/surveydata/2018_survey_data.anonymised.csv','./2018-data.csv')
}
raw_csv <- read.csv('./2018-data.csv')

vars <- c('Any.other.feedback.about.our.support.and.documentation.',
          'What.s.the.most.important.thing.Foreman.solves.for.you..today..',
          'What.expansion.plans.do.you.have.for.Foreman.in.the.next.year.',
          'What.s.the.most.important.thing.you.d.like.to.see.done.next.in.Foreman.',
          'Do.you.have.any.additional.comments.or.special.requests..What.pain.points.should.be.addressed.',
          'Any.comments.or.suggestions.you.d.like.to.make.about.Hammer.',
          'Any.comments.or.suggestions.you.d.like.to.make.about.the.API.',
          'How.do.you.manage.your.content.',
          'What.s.your.biggest.problem.blocker.with.contributing.to.Foreman.that.we.need.to.solve.')

writeout <- function(colname) {
  d1 <- as_tibble(raw_csv[colname])
  names(d1) <- c('Answer')
  d1 %>%
    filter(Answer != "") %>%            # remove blank
    distinct(Answer) %>%                # remove duplicates
    arrange(as.character(Answer)) %>%   # sort alphabetically
    mutate(ID=1:n()) %>%                # number rows
    select(ID,Answer)                   # reorder columns
}

for (n in seq(1,length(vars))) {
  d2 <- writeout(vars[n])
  filename <- paste('/tmp/csv/',sprintf('%02d',n),'-',vars[n],'.csv',sep = '')
  write.csv(x = d2, file = filename,row.names = F)
}

# Now process the CSV files with `csvtomd` from PIP
# csvtomd /tmp/csv/*csv
